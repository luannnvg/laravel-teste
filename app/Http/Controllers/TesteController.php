<?php

namespace App\Http\Controllers;

use thiagoalessio\TesseractOCR\TesseractOCR;

class TesteController extends Controller
{

    public function index()
    {
        // $input  = new TesseractOCR('/home/infocast/Downloads/imagemtexto.png');
        $input     = new TesseractOCR('/home/infocast/Downloads/teste.jpg');
        $output    = $input->run();
        $altern    = str_replace("\n", "lineProx", $output);
        $arrayText = explode('lineProx', $altern);
        $user      = '';
        $result    = [
            'success'  => false,
            'data'     => null,
            'messages' => [
                'success' => 'Usuario Encontrado',
                'error'   => 'Usuario Não Encontrado',
            ],
            'error'    => [
                'state' => false,
                'line'  => null,
                'file'  => null,
            ],
        ];
        //rejeita os que nao tem @ ou que não tem insta
        $response = collect($arrayText)->reject(function ($name) {
            $valid1 = $this->validString($name, '@');
            $valid2 = $this->validString($name, 'Insta');
            return !$valid1 || !$valid2;
        });
        $line      = $response->first();
        $arrayInfo = explode(' ', $line);

        //pega a segunda posição do array pois a primeira e o texto de origem de aplicativo
        if (collect($arrayInfo)->count() >= 2) {
            $user = $arrayInfo[1];
        }

        $user = !$this->validString($user, '@') ? '@' . $user : $user;

        if (!empty($user)) {
            $result['success'] = true;
            $result['data']    = $user;
        }

        return $result;
    }

    public function validString($string, $search)
    {
        $pattern1 = '/' . $search . '/'; //Padrão a ser encontrado na string $tags
        return preg_match($pattern1, $string);
    }
}
